//commende mitraillette royal les miniatures

//Info venant de Rémond
const int pinContactOn = 2;// info de mise attente -- sortie de culasse
const int pinCycleOn = 3;//mise en marche marche des mitraillettes

//mitrallette 1
const int c1 = 4;//culasse 1
const int p1 = 5;//piston 1
const int ej1 = 6;//ejection 1
const int el1 = 7;//electrovanne 1

//mitrallette 2
const int c2 = 8;
const int p2 = 9;
const int ej2 = 10;
const int el2 = 11;

int t0 = 400;//
int t1 = 200;//
int t2 = 200;//

//bool 1cycle = false;


void setup() {
  //Serial.begin (9600);

  pinMode (pinContactOn, INPUT_PULLUP);
  pinMode (pinCycleOn, INPUT_PULLUP);

  pinMode(c1, OUTPUT);
  pinMode(p1, OUTPUT);
  pinMode(ej1, OUTPUT);
  pinMode(el1, OUTPUT);

  pinMode(c2, OUTPUT);
  pinMode(p2, OUTPUT);
  pinMode(ej2, OUTPUT);
  pinMode(el2, OUTPUT);

}

void loop() {

  int contactOn = digitalRead (pinContactOn);//read pinContact

  if ( !contactOn) {//************Contact ON
    //Serial.println("Contact ON");

    digitalWrite(c1, HIGH); digitalWrite(p1, LOW); digitalWrite(ej1, LOW); digitalWrite(el1, LOW);
    digitalWrite(c2, HIGH); digitalWrite(p2, LOW); digitalWrite(ej2, LOW); digitalWrite(el2, LOW);
    delay (t1+t2);

    bool cycleOn = digitalRead (pinCycleOn); //read pinCycle
    bool firstCycle = true;
    while (! cycleOn) {//**********Cycle On
      //Serial.println("Cycle ON");

      if (firstCycle) {//*****1er CycleON
        //Serial.println("first ON");

        digitalWrite(c1, HIGH); digitalWrite(p1, HIGH); digitalWrite(ej1, LOW); digitalWrite(el1, LOW);
        digitalWrite(c2, HIGH); digitalWrite(p2, LOW); digitalWrite(ej2, LOW); digitalWrite(el2, LOW);
        delay (t1+t2);

        firstCycle = false;
      }

      else {
        //Serial.println("cylce routine");

        //********step 1
        digitalWrite(c1, LOW); digitalWrite(p1, HIGH); digitalWrite(ej1, HIGH); digitalWrite(el1, HIGH);
        digitalWrite(c2, HIGH); digitalWrite(p2, HIGH); digitalWrite(ej2, LOW); digitalWrite(el2, LOW);
        delay (t1);

        //********step 2
        digitalWrite(c1, HIGH); digitalWrite(p1, LOW); digitalWrite(ej1, LOW); digitalWrite(el1, LOW);
        digitalWrite(c2, HIGH); digitalWrite(p2, HIGH); digitalWrite(ej2, LOW); digitalWrite(el2, LOW);
        delay (t2);

        //********step 3
        digitalWrite(c1, HIGH); digitalWrite(p1, HIGH); digitalWrite(ej1, LOW); digitalWrite(el1, LOW);
        digitalWrite(c2, LOW); digitalWrite(p2, HIGH); digitalWrite(ej2, HIGH); digitalWrite(el2, HIGH);
        delay (t1);

        //********step 4
        digitalWrite(c1, HIGH); digitalWrite(p1, HIGH); digitalWrite(ej1, LOW); digitalWrite(el1, LOW);
        digitalWrite(c2, HIGH); digitalWrite(p2, LOW); digitalWrite(ej2, LOW); digitalWrite(el2, LOW);
        delay (t2);

      }
      cycleOn = digitalRead (pinCycleOn);
    }
  }

  else { //************Contact Off**********
   // Serial.println("Contact off");

    digitalWrite(c1, LOW); digitalWrite(p1, LOW); digitalWrite(ej1, LOW); digitalWrite(el1, LOW);
    digitalWrite(c2, LOW); digitalWrite(p2, LOW); digitalWrite(ej2, LOW); digitalWrite(el2, LOW);
    
  }
}
